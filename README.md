# AI4AI

## QUICK START

Assuming docker is installed on your computer and this is your working directory,
execute in a shell:

    $ docker-compose up --build

Wait until the build has completed.
From a web browser with MetaMask as plugin, access:

    http://localhost:8080

If you get an empty page, wait a little longer then try accessing the url again
